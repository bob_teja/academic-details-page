import React, { Component } from 'react';

class AcademicDetailsCard extends Component {

    submitButtonClicked = () => {
        console.log("Submit Button clicked")
    }
    render() {
        const {board,name,passoutYear,percentage,displaySubmit} = this.props;
        return(
            <div>
                Board : {board + " "}
                Name : {name + " "}
                Passout Year : {passoutYear + " "}
                Percentage : {percentage + " "}
                {(displaySubmit)
                    ? <button onClick={this.submitButtonClicked}>Submit Details</button>
                    : <></>
                }
            </div>
        );
    }
}

export default AcademicDetailsCard;