import React, { Component } from 'react';

import {connect} from 'react-redux';

import '../css/App.css'
import AcademicDetailsCard from './AcademicDetailsCard';

class App extends Component {
  

  render() {
    const {board, name, percentage, passoutYear} = this.props.academicDetailsReducer;
    return (
      <div className="App">
          {
            (board === null)
            ?<p>No details Found</p>
            : board.map((board,index)=>
            <AcademicDetailsCard key = {Math.floor(Math.random()*100)} 
                                 board = {board} 
                                 name = {name[index]}
                                 percentage = {percentage[index]}
                                 passoutYear = {passoutYear[index]}
                                 displaySubmit = {(index === board.length-1) ? true : false}
                                 />)
          }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    academicDetailsReducer: state.academicDetailsReducer
  };
}

export default connect(mapStateToProps)(App);
