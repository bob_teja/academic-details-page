import ACTION from "../actions/actionTypes"
import initialState from "../store/initialState";

export default function(state = initialState,action) {
    switch (action.type) {
        case ACTION.ADD_STUDENT_DETAILS : {
            return {
                academicDetails: action.payload
            }
        }
        default: return state;
    }
}