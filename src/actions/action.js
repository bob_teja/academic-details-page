import ACTION from "./actionTypes"

export const setStudentDetails = (studentDetails) => {
    return {
        type: ACTION.ADD_STUDENT_DETAILS,
        payload: studentDetails
    }
};
