const initialState = {
    board : ['JNTU','BIE','SSC'],
    name : ['VNR','S Gayatri','Rao\'s'],
    percentage : [86.6,96.4,9.2],
    passoutYear : [2018,2014,2012]
};

export default initialState;

/*

['JNTU','BIE','SSC']
*/