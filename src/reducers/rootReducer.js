import {combineReducers} from 'redux';
import AcademicDetailsReducer from  "./academicDetailsReducer"

const allReducers = combineReducers({
    academicDetailsReducer: AcademicDetailsReducer
});

export default allReducers;